package ru.casperix.signals.concrete

import ru.casperix.signals.api.AbstractSignal

/**
 * 	Сигнал ожидания события
 * 	Оповещает всех подписчиков при установке
 * 	Аналогичен [Signal] но не содержит сообщения
 */
class EmptySignal : EmptyPromise, AbstractSignal<() -> Unit, Slot>(SlotCollection()) {
	override fun set() {
		forEach { listener ->
			listener()
		}
	}
}