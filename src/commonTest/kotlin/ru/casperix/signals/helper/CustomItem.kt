package ru.casperix.signals.helper

class CustomItem {
	val results = mutableListOf<Int>()

	fun action(item: Int) {
		results.add(item)
	}
}