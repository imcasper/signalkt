package ru.casperix.signals.concrete

import ru.casperix.misc.*
import ru.casperix.signals.api.CustomFuture

class EitherCollectionFuture<Accept, Reject>(loaders: List<EitherFuture<Accept, Reject>>) :
    EitherFuture<List<Accept>, List<Reject>> {

    constructor(vararg loaders: EitherFuture<Accept, Reject>) : this(loaders.toList())

    private val resultMap: MutableList<Either<Accept, Reject>?> = loaders.map {
        null
    }.toMutableList()

    private val outputSignal = EitherSignal<List<Accept>, List<Reject>>()

    override val acceptFuture: CustomFuture<(List<Accept>) -> Unit, Slot> = outputSignal.acceptFuture
    override val rejectFuture: CustomFuture<(List<Reject>) -> Unit, Slot> = outputSignal.rejectFuture
    override val complete: Boolean get() = outputSignal.complete

    init {
        loaders.forEachIndexed { index, future ->
            future.thenAccept {
                resultMap[index] = Left(it)
                outputIfCompleted()
            }
            future.thenReject {
                resultMap[index] = Right(it)
                outputIfCompleted()
            }
        }
    }

    private fun outputIfCompleted() {
        val listA: List<Accept> = resultMap.mapNotNull {
            if (it == null) return
            it.left()
        }
        val listB: List<Reject> = resultMap.mapNotNull {
            if (it == null) return
            it.right()
        }

        if (listB.isNotEmpty()) {
            outputSignal.reject(listB)
        } else {
            if (listA.size != resultMap.size) {
                throw Exception("Invalid accept result")
            }
            outputSignal.accept(listA)
        }
    }
}