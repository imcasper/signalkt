package ru.casperix.signals.api

interface CustomSlotCollection<Listener, Slot> : CustomFuture<Listener, Slot> {
	val size:Int
	fun forEach(action:(Listener)->Unit)
	fun getListener(slot: Slot):Listener?
	fun removeAllListeners()
}