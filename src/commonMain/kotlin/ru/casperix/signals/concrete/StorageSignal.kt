package ru.casperix.signals.concrete

import ru.casperix.signals.api.AbstractSignal

/**
 * 	Хранит значение, и срабатывает при изменении значения
 *
 * 	See also [Signal]
 *
 * 	@param ignoreEqual        игнорировать новое значение при установке, если оно эквивалентно старому
 * 	@param dispatchOnThen        слушатель уведомляется при подписке
 */
class StorageSignal<Event>(value: Event) : StoragePromise<Event>, AbstractSignal<(Event) -> Unit, Slot>(SlotCollection()) {
	override var value: Event = value
		set(value: Event) {
			if (field != value) {
				field = value

				forEach { listener ->
					listener(value)
				}
			}
		}

	/**	For compatible	with previous version*/
	override fun then(listener: (Event) -> Unit): Slot {
		return super.then(listener)
	}

	fun thenAndNow(listener: (Event) -> Unit): Slot {
		listener(value)
		return super.then(listener)
	}

	override fun set(value: Event) {
		this.value = value
	}
}