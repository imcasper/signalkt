package ru.casperix.signals.collection

import ru.casperix.signals.concrete.Future
import ru.casperix.signals.concrete.Slot

interface ObservableMap<Key, Value> {
	val keys: MutableSet<Key>
	val values: MutableCollection<Value>

	fun addFuture(): Future<Map.Entry<Key, Value>>
	fun removeFuture(): Future<Map.Entry<Key, Value>>

	fun then(addListener: (Map.Entry<Key, Value>) -> Unit, removeListener: (Map.Entry<Key, Value>) -> Unit): Pair<Slot?, Slot?> {
		return Pair(
				addFuture().then(addListener),
				removeFuture().then(removeListener)
		)
	}


	fun cancel(forAdd: Slot?, forRemove: Slot?) {
		forAdd?.let { addFuture().cancel(it) }
		forRemove?.let { removeFuture().cancel(it) }
	}

	fun thenAdd(listener: (Map.Entry<Key, Value>) -> Unit): Slot? {
		return addFuture().then(listener)
	}

	fun thenRemove(listener: (Map.Entry<Key, Value>) -> Unit): Slot? {
		return removeFuture().then(listener)
	}

	fun thenAddKey(listener: (Key) -> Unit): Slot? {
		return addFuture().then { listener(it.key) }
	}

	fun thenRemoveKey(listener: (Key) -> Unit): Slot? {
		return removeFuture().then { listener(it.key) }
	}

	fun thenAddValue(listener: (Value) -> Unit): Slot? {
		return addFuture().then { listener(it.value) }
	}

	fun thenRemoveValue(listener: (Value) -> Unit): Slot? {
		return removeFuture().then { listener(it.value) }
	}
}