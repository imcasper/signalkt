package ru.casperix.signals.concrete

import ru.casperix.signals.collection.IterateSafeMap
import ru.casperix.signals.api.CustomSlotCollection

/**
 * 	Реализация коллекции слотов для сигналов
 */
class SlotCollection<Listener> : CustomSlotCollection<Listener, Slot> {
	protected val slots = IterateSafeMap<Long, Listener>()

	private var ID = 0L
	private var onCancel = ::cancel

	override fun forEach(action: (Listener) -> Unit) {
		slots.forEachValue(action)
	}

	override fun removeAllListeners() {
		slots.clear()
	}

	override fun getListener(slot: Slot): Listener? {
		return slots[slot.id]
	}

	override fun cancel(slot: Slot): Boolean {
		return slot.holder === this && slots.remove(slot.id) != null
	}

	override fun then(listener: Listener): Slot {
		val slot = Slot(++ID, this, onCancel)
		slots[slot.id] = listener
		return slot
	}

	override val size: Int
		get() = slots.size
}