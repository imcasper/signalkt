package ru.casperix.signals.concrete

import ru.casperix.misc.Disposable


/**
 * 	Реализация слота для системы сигналов
 */
class Slot(internal val id: Long, internal val holder: Any, internal val onRemove: (Slot) -> Boolean) : Disposable {
	override fun dispose() {
		onRemove(this)
	}
}
