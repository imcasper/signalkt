package ru.casperix.signals.api

interface CustomSingleFuture<Listener, Slot> : CustomFuture<Listener, Slot>, CustomSingle