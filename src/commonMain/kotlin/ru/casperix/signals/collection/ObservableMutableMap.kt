package ru.casperix.signals.collection

import ru.casperix.signals.concrete.Future
import ru.casperix.signals.concrete.Signal

class ObservableMutableMap<Key, Value> : MutableMap<Key, Value>, ObservableMap<Key, Value> {
	private val map = mutableMapOf<Key, Value>()

	private val addSignal = Signal<Map.Entry<Key, Value>>()
	private val removeSignal = Signal<Map.Entry<Key, Value>>()

	override fun addFuture(): Future<Map.Entry<Key, Value>> {
		return addSignal
	}

	override fun removeFuture(): Future<Map.Entry<Key, Value>> {
		return removeSignal
	}


	override fun get(key: Key): Value? {
		return map[key]
	}

	override fun remove(key: Key): Value? {
		val element = map.remove(key) ?: return null
		removeSignal.set(object : Map.Entry<Key, Value> {
			override val key = key
			override val value = element
		})
		return element
	}

	override val entries: MutableSet<MutableMap.MutableEntry<Key, Value>>
		get() = map.entries

	override val keys: MutableSet<Key>
		get() = map.keys

	override val values: MutableCollection<Value>
		get() = map.values

	override fun containsKey(key: Key): Boolean {
		return map.containsKey(key)
	}

	override fun containsValue(value: Value): Boolean {
		return map.containsValue(value)
	}

	override fun isEmpty(): Boolean {
		return map.isEmpty()
	}

	override val size: Int
		get() = map.size

	override fun clear() {
		entries.forEach {
			removeSignal.set(it)
		}
		map.clear()
	}

	override fun put(key: Key, value: Value): Value? {
		if (map.containsKey(key)) return null
		map[key] = value
		addSignal.set(object : Map.Entry<Key, Value> {
			override val key = key
			override val value = value
		})
		return value
	}

	override fun putAll(from: Map<out Key, Value>) {
		from.forEach {
			put(it.key, it.value)
		}
	}

//	override fun contains(element: Map.Entry<Key, Value>): Boolean {
//		return map.get(element.key) == element.value
//	}
//
//	override fun containsAll(elements: Collection<Map.Entry<Key, Value>>): Boolean {
//		elements.forEach {
//			if (!contains(it)) return false
//		}
//		return true
//	}

//	override fun iterator(): Iterator<Map.Entry<Key, Value>> {
//		return map.iterator()
//	}
}