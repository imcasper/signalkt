package ru.casperix.signals.concrete

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

class NotifyProperty<Custom>(private var self: Custom, private val signal: EmptySignal) : ReadWriteProperty<Any, Custom> {

	override fun getValue(thisRef: Any, property: KProperty<*>): Custom {
		return self
	}

	override fun setValue(thisRef: Any, property: KProperty<*>, value: Custom) {
		if (self == value) return
		self = value
		signal.set()
	}
}