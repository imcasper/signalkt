package ru.casperix.signals.util

import ru.casperix.signals.collection.EitherCollection
import ru.casperix.signals.concrete.EitherSignal
import kotlin.test.Test
import kotlin.test.assertEquals

class EitherCollectionTest {

	@Test
	fun simple() {
		val p1  = EitherSignal<String, String>()
		val p2  = EitherSignal<String, String>()
		val p3  = EitherSignal<String, String>()

		var result = ""

		val u1 = EitherCollection()
		u1.add(p1)
		u1.add(p2)

		val f1 = u1.future()
		f1.thenAccept {
			result += "u1+"
		}
		f1.thenReject {
			result += "u1-"
		}
		val u2 = EitherCollection()
		u2.add(p1)
		u2.add(p2)
		u2.add(p3)

		val f2 = u2.future()
		f2.thenAccept {
			result += "u2+"
		}
		f2.thenReject {
			result += "u2-"
		}

		p1.acceptDispatcher("")
		assertEquals("", result)

		p2.acceptDispatcher("")
		assertEquals("u1+", result)

		p3.rejectDispatcher("")
		assertEquals("u1+u2-", result)
	}
}