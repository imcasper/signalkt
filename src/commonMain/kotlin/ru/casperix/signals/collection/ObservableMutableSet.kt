package ru.casperix.signals.collection

import ru.casperix.signals.concrete.Future
import ru.casperix.signals.concrete.Signal

class ObservableMutableSet<Item> : MutableSet<Item>, ObservableCollection<Item> {
	private val items = mutableSetOf<Item>()

	private val addSignal = Signal<Item>()
	private val removeSignal = Signal<Item>()

	override fun addFuture(): Future<Item> {
		return addSignal
	}

	override fun removeFuture(): Future<Item> {
		return removeSignal
	}

	override fun add(element: Item): Boolean {
		if (items.add(element)) {
			addSignal.set(element)
			return true
		}
		return false
	}

	override fun remove(element: Item): Boolean {
		if (items.remove(element)) {
			removeSignal.set(element)
			return true
		}
		return false
	}

	override fun addAll(elements: Collection<Item>): Boolean {
		var result = false
		for (element in elements) {
			val next = add(element)
			result = result || next
		}
		return result
	}

	override fun clear() {
		removeAll(items.toList())
	}

	override fun iterator(): MutableIterator<Item> {
		return items.iterator()
	}

	override fun removeAll(elements: Collection<Item>): Boolean {
		var result = false
		for (element in elements) {
			val next = remove(element)
			result = result || next
		}
		return result
	}

	override fun retainAll(elements: Collection<Item>): Boolean {
		throw Error("No implementation")
	}

	override val size: Int
		get() = items.size

	override fun contains(element: Item): Boolean {
		return items.contains(element)
	}

	override fun containsAll(elements: Collection<Item>): Boolean {
		return items.containsAll(elements)
	}

	override fun isEmpty(): Boolean {
		return items.isEmpty()
	}
}