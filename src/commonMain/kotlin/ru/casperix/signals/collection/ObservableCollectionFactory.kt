package ru.casperix.signals.collection

fun <T> observableListOf(vararg items: T): ObservableMutableList<T> {
	val result = ObservableMutableList<T>()
	result.addAll(items)
	return result
}

fun <T> observableUniqueListOf(vararg items: T): ObservableMutableList<T> {
	val result = ObservableMutableList<T>(true)
	result.addAll(items)
	return result
}

fun <T> observableSetOf(vararg items: T): ObservableMutableSet<T> {
	val result = ObservableMutableSet<T>()
	result.addAll(items)
	return result
}

fun <K, V> observableMapOf(vararg pairs: Pair<K, V>): ObservableMutableMap<K, V> {
	val result = ObservableMutableMap<K, V>()
	pairs.forEach {
		result.put(it.first, it.second)
	}
	return result
}


fun <T> List<T>.toObservableList(): ObservableMutableList<T> {
	val result = ObservableMutableList<T>()
	result.addAll(this)
	return result
}


fun <T> List<T>.toUniqueObservableList(): ObservableMutableList<T> {
	val result = ObservableMutableList<T>(true)
	result.addAll(this)
	return result
}

fun <K, V> Map<K, V>.toObservableMap(): ObservableMutableMap<K, V> {
	val result = ObservableMutableMap<K, V>()
	result.putAll(this)
	return result
}

