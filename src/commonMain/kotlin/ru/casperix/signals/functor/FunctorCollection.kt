package ru.casperix.signals.functor

import ru.casperix.signals.api.CustomSlotCollection

class FunctorCollection<Listener : Functor<Event>, Event> : CustomSlotCollection<Listener, Listener> {
	private val listeners = mutableSetOf<Listener>()

	override fun cancel(slot: Listener): Boolean {
		return listeners.remove(slot)
	}


	override fun getListener(slot: Listener): Listener? {
		return listeners.firstOrNull { it == slot }
	}



	override fun removeAllListeners() {
		listeners.clear()
	}

	override fun then(listener: Listener): Listener {
		listeners.add(listener)
		return listener
	}

	override val size: Int
		get() = listeners.size

	override fun forEach(action: (Listener) -> Unit) {
		listeners.forEach(action)
	}

}