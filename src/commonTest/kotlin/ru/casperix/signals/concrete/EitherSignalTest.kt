package ru.casperix.signals.concrete

import kotlin.test.Test
import kotlin.test.assertEquals

class EitherSignalTest {
	@Test
	fun testSet() {
		var results = ""

		val eitherA = EitherSignal<Int, String>()
		eitherA.thenAccept { results += ("int-$it;") }
		eitherA.thenReject { results += ("str-$it;") }

		eitherA.acceptDispatcher(11)
		eitherA.rejectDispatcher("ignored")
		eitherA.acceptDispatcher(22)

		val eitherB = EitherSignal<Int, String>()
		eitherB.thenAccept { results += ("int-$it;") }
		eitherB.thenReject { results += ("str-$it;") }

		eitherB.rejectDispatcher("fail")
		eitherB.rejectDispatcher("ignored")
		eitherB.acceptDispatcher(44)


		assertEquals("int-11;str-fail;", results)
	}

	@Test
	fun simple() {
		val builder = StringBuilder()
		val def = EitherSignal<Int, String>()

		test(builder, EitherFuture.accept(1))//completed
		test(builder, EitherFuture.reject("_"))//completed
		test(builder, def)//wait

		assertEquals("1_", builder.toString())

		def.accept(22)//completed
		assertEquals("1_22", builder.toString())
	}

	private fun test(builder: StringBuilder, future: EitherFuture<Int, String>) {
		future.then({
			builder.append(it)
		}, {
			builder.append(it)
		})
	}
}