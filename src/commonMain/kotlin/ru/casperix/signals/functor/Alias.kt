package ru.casperix.signals.functor

import ru.casperix.misc.Disposable
import ru.casperix.signals.api.CustomFuture
import ru.casperix.signals.api.CustomPromise
import ru.casperix.signals.api.CustomStorageFuture
import ru.casperix.signals.api.CustomStoragePromise

interface Functor<Event> {
	fun receiver(event: Event)
}

typealias FunctorFuture<Listener> = CustomFuture<Listener, Listener>
interface FunctorStorageFuture<Listener, Event>:  FunctorFuture<Listener>, CustomStorageFuture<Listener, Event, Listener>
interface FunctorStoragePromise<Listener, Event>:  FunctorPromise<Listener, Event>, FunctorStorageFuture<Listener, Event>,
    CustomStoragePromise<Listener, Event, Listener>

interface FunctorPromise<Listener, Event> : CustomPromise<Listener, Listener> {
	fun send(value: Event)
}

typealias ObjectListener = Functor<Any>
typealias ObjectFuture = FunctorFuture<Functor<Any>>
typealias ObjectPromise = FunctorPromise<Functor<Any>, Any>
typealias ObjectSignal = FunctorSignal<Functor<Any>, Any>
typealias ObjectStorageSignal = FunctorStorageSignal<Functor<Any>, Any>


fun ObjectFuture.then(components:MutableCollection<in Disposable>, listener: ObjectListener) {
	components.add(object :Disposable{
		override fun dispose() {
			cancel(listener)
		}
	})
	then(listener)
}
