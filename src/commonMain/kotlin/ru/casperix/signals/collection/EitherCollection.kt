package ru.casperix.signals.collection

import ru.casperix.signals.concrete.EitherFuture
import ru.casperix.signals.concrete.EitherSignal

class EitherCollection {
	class Result(val promise: Any, val result:Any?)

	private var block = true
	private var amount = 0
	private val common = EitherSignal<List<Result>, List<Result>>()
	private val acceptList = mutableListOf<Result>()
	private val rejectList = mutableListOf<Result>()

	fun <Accept : Any, Reject : Any> add(promise: EitherFuture<Accept, Reject>) {
		amount++
		promise.then({ onAccept(promise, it) }, { onReject(promise, it) })
	}

	fun future(): EitherFuture<List<Result>, List<Result>> {
		block = false
		check()
		return common
	}

	private fun onAccept(promise: Any, value: Any?) {
		acceptList.add(Result(promise, value))
		check()
	}

	private fun onReject(promise:Any, value: Any?) {
		rejectList.add(Result(promise, value))
		check()
	}

	private fun check() {
		if (!block && acceptList.size + rejectList.size == amount) {
			complete()
		}
	}

	private fun complete() {
		if (rejectList.isNotEmpty()) {
			common.rejectDispatcher(rejectList)
		} else {
			common.acceptDispatcher(acceptList)
		}
	}
}