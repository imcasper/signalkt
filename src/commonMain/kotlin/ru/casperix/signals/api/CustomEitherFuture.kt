package ru.casperix.signals.api

import ru.casperix.signals.concrete.EitherSignal
import ru.casperix.signals.concrete.Slot


interface CustomEitherFuture<AcceptListener, RejectListener, Slot> {
    val complete: Boolean

    val acceptFuture: CustomFuture<AcceptListener, Slot>
    val rejectFuture: CustomFuture<RejectListener, Slot>

    fun then(accept: AcceptListener, reject: RejectListener): Pair<Slot?, Slot?> {
        return Pair(
            acceptFuture.then(accept),
            rejectFuture.then(reject)
        )
    }

    fun thenAccept(accept: AcceptListener): Slot? {
        return acceptFuture.then(accept)
    }

    fun thenReject(reject: RejectListener): Slot? {
        return rejectFuture.then(reject)
    }

    companion object {
        fun <Accept, Reject> accept(value: Accept): CustomEitherPromise<(Accept) -> Unit, (Reject) -> Unit, Slot> {
            return EitherSignal<Accept, Reject>().apply { accept(value) }
        }
        fun <Accept, Reject> reject(value: Reject): CustomEitherPromise<(Accept) -> Unit, (Reject) -> Unit, Slot> {
            return EitherSignal<Accept, Reject>().apply { reject(value) }
        }
    }
}

