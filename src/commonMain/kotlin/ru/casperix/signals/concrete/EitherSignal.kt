package ru.casperix.signals.concrete

import ru.casperix.signals.api.CustomFuture

/**
 * 	Сигнал ожидания события: "успех" ИЛИ "неудача"
 * 	Событие происходит только однажды и состояние фиксируется.
 *
 * 	Вы можете подписаться в любой момент
 * 	Слушатель получит свое значение когда оно появится (или немедленно, если сигнал уже сработал)
 *
 * 	Удобно использовать когда есть два варианта исхода, например загрузка ассетов
 *
 * 	val loadSignal = EitherSignal<Image, ImageLoadError>()
 * 	...
 * 	loadSignal.accept(Image())
 * 	loadSignal.reject(ImageLoadError())
 * 	...
 * 	loadSignal.thenAccept{
 * 		drawImage(it)
 * 	}
 * 	loadSignal.thenReject {
 * 		printError(it)
 * 	}
 *
 */
class EitherSignal<Accept, Reject> : EitherPromise<Accept, Reject> {
	private val acceptSignal = SingleSignal<Accept>()
	private val rejectSignal = SingleSignal<Reject>()
	override var complete = false; private set

	override fun accept(accept: Accept) {
		if (!complete) {
			complete = true
			acceptSignal.set(accept)
		}
	}

	override fun reject(reject: Reject) {
		if (!complete) {
			complete = true
			rejectSignal.set(reject)
		}
	}

	fun dropResult() {
		acceptSignal.dropResult()
		rejectSignal.dropResult()
		complete = false
	}

	override val acceptDispatcher: (Accept) -> Unit = ::accept
	override val rejectDispatcher: (Reject) -> Unit = ::reject

	override val acceptFuture: CustomFuture<(Accept) -> Unit, Slot> = acceptSignal

	override val rejectFuture: CustomFuture<(Reject) -> Unit, Slot> = rejectSignal
}