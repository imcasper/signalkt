package ru.casperix.signals.concrete

import ru.casperix.signals.api.AbstractSignal

/**
 * 	Срабатывает один раз, не содержит сообщения
 */
class EmptySingleSignal : EmptySinglePromise, AbstractSignal<() -> Unit, Slot>(SlotCollection()) {
	override var completed = false; private set

	override fun then(listener: () -> Unit): Slot {
		if (completed) {
			listener()
		}
		return super.then(listener)
	}

	override fun set() {
		if (!completed) {
			completed = true

			forEach { listener ->
				listener()
			}
		}
	}

	fun dropResult() {
		completed = false
	}
}